<?php

use Core\View;
use Core\Language;

?>

<h2 class="title"><?php echo($data['heading']); ?></h2>

<p>Select a tag to view its statistics.</p>

<form method="GET" action="" class="pure-form pure-form-aligned">
	<label for="newtag-tagid">Tag </label>
	<select name="tagid" id="newtag-tagid">
		<?php foreach ($data['tagList'] as $parentTag) : ?>

			<option value="<?php echo($parentTag['tagID']); ?>" <?php echo ($data['selectedTag']['tagID'] == $parentTag['tagID']) ? 'selected' : ''; ?>>
			<?php echo($parentTag['tagName']); ?>
			</option>

			<?php foreach ($parentTag['subTags'] as $subTag) : ?>
				<option value="<?php echo($subTag['tagID']); ?>" <?php echo ($data['selectedTag']['tagID'] == $subTag['tagID']) ? 'selected' : ''; ?>>
				&nbsp;&nbsp;&nbsp;<?php echo($subTag['tagName']); ?>
				</option>
			<?php endforeach; ?>

		<?php endforeach; ?>
	</select>
	<input type="submit" value="View" class="pure-button pure-button-primary">
</form>


<?php if(isset($data['tagInfo']) && count($data['tagInfo']) > 0) : ?>

<hr />

<div id="tabs" class="hidden">
	<ul>
		<li><a href="#tabs-1">Breakdown</a></li>
		<li><a href="#tabs-2">Comments</a></li>
	</ul>
	<div id="tabs-1">

		<h3 class="title">Analysis for "<?php echo($data['tagInfo']['tagName']); ?>"</h3>
		<div class="pure-g">
		    <div class="pure-u-1-2">
		    	<p>This Month</p>
		    	<p>Total number of tags: <?php echo $data['tagCountThisMonth']['total']; ?></p>
		    	<p>Number of Positive tags: <?php echo $data['tagCountThisMonth']['positiveCount']; ?></p>
		    	<p>Number of Negative tags: <?php echo $data['tagCountThisMonth']['negativeCount']; ?></p>
		    	<p>Number of Neutral tags: <?php echo $data['tagCountThisMonth']['neutralCount']; ?></p>
		    </div>

		    <div class="pure-u-1-2">
		    	<p>All Time</p>
		    	<p>Total number of tags: <?php echo $data['tagInfo']['totalCount']; ?></p>
		    	<p>Number of Positive tags: <?php echo $data['tagInfo']['totalPositiveCount']; ?></p>
		    	<p>Number of Negative tags: <?php echo $data['tagInfo']['totalNegativeCount']; ?></p>
		    	<p>Number of Neutral tags: <?php echo $data['tagInfo']['totalNeutralCount']; ?></p>
		    </div>
		</div>

		<?php
		if(count($data['tagInfo']) > 0)
		View::render('analysis/dynamiccharts', $data['chartData']);
		?>


	</div> <!-- end tabs-1-->

	<div id="tabs-2">
		<table class="pure-table pure-table-horizontal datatables-full" style="width:96%">

			<thead>
				<td>Time</td>
				<td>Student Number</td>
				<td>Comment</td>
				<td>Location</td>
				<td>Options</td>
			</thead>

			<tbody>
				<?php foreach($data['tagComments'] as $comment): ?>
				<tr>
					<td><?php echo( date('d/m/Y', $comment['timestamp'])); ?></td>
					<td><?php echo($comment['studentNumber']); ?></td>
					<td><?php echo($comment['comment']); ?></td>
					<td><?php echo($comment['locationName']); ?></td>
					<td>
						<form action="tagcomment" method="GET">
							<input type="hidden" name="commentID" value="<?php echo($comment['commentID']); ?>">
							<input type="submit" value="View" class="pure-button pure-button-primary" style="width:70px;">
						</form>
						<form action="" method="POST">
							<input type="hidden" name="commentid" value="<?php echo($comment['commentID']); ?>">
							<input type="submit" value="Delete" name="deletecomment" class="pure-button pure-button-primary" style="width:70px; margin-top:3px;" 
								onclick="return confirm('This will permanently delete the comment and remove any tags that have been attached to it.\n\nAre you sure you want to delete it?', 'yes');">
						</form>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div> <!-- end tabs-2 -->
</div> <!-- end tabs -->

<?php elseif(!empty($data['selectedTag'])): ?>

<hr />
<p>There are currently no tags set for <?php echo($data['selectedTag']['tagName']); ?>.</p>

<?php else: ?>
<hr />
<p>Please select a tag.</p>
<?php endif; ?>


