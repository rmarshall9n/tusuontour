
<center>
<?php
foreach ($data['chartDisplay'] as $chartDisplay) :
	if($chartDisplay['span'] == 2) :
?>
		<label for="<?php echo $chartDisplay['chartID']; ?>">
		<?php echo $chartDisplay['title']; ?><br />
		<canvas id="<?php echo $chartDisplay['chartID']; ?>" class="chartjs-2"></canvas>
		</label>
<?php
	elseif($chartDisplay['span'] == 1) :
?>
	<span>
		<label for="<?php echo $chartDisplay['chartID']; ?>">
		<?php echo $chartDisplay['title']; ?><br />
		<canvas id="<?php echo $chartDisplay['chartID']; ?>" class="chartjs-1" height="200px"></canvas>
		</label>
	</span>
<?php
	endif;
endforeach;
?>
</center>

<script>
<?php
	$jsData = json_encode($data['chartData']);
	echo "var chartData = ".$jsData.";\n";
?>
</script>
