<?php

use Core\Language;

?>

<h2 class="title"><?php echo($data['heading']); ?></h2>

<form method="POST" id="SubmitForm" class="pure-form pure-form-aligned">
<fieldset>
	<input type="hidden" id="comment-id" name="comment-id" value="<?php echo $data['commentInfo']['commentID']; ?>">
	
	<!-- Student Number -->
	<div class="pure-control-group">
		<label for="studentNo">Student Number</label>
		<input id="studentNo" name="studentNo" type="text" value="<?php echo($data['commentInfo']['studentNumber']); ?>" placeholder="Student Number" readonly>
	</div>

	<!-- Comment -->
	<div class="pure-control-group">
		<label for="comment">Comment</label>
		<textarea id="comment" name="comment" placeholder="Comment" required style="width:500px; height:300px" <?php echo (!$data['newComment'] ? 'readonly' : ''); ?>><?php echo($data['commentInfo']['comment']); ?></textarea>
	</div>

	<!-- Location -->
	<div class="pure-control-group">
		<label for="Location">Location</label>
		<select id="Location" name="locationID">

			<?php foreach ($data['locations'] as $location) : ?>
			<option value="<?php echo($location['locationID']); ?>"<?php if($data['locationID'] == $location['locationID']){echo("selected");};?>><?php echo($location['locationName']); ?></option>
			<?php endforeach; ?>

		</select>
	</div>
	<br />



	<!-- Add tag -->
	<div class="pure-control-group">

		<!-- Tag Name -->
		<label for="newtag-tagid">Tag</label>
		<select name="newtag-tagid" id="newtag-tagid">
			<?php foreach ($data['tagList'] as $parentTag) : ?>

				<option value="<?php echo($parentTag['tagID']); ?>"><?php echo($parentTag['tagName']); ?></option>

				<?php foreach ($parentTag['subTags'] as $subTag) : ?>
					<option value="<?php echo($subTag['tagID']); ?>">&nbsp;&nbsp;&nbsp;<?php echo($subTag['tagName']); ?></option>
				<?php endforeach; ?>

			<?php endforeach; ?>
		</select>



		<!-- Tag Characteristic -->
		<select name="newtag-characteristicid" id="newtag-characteristicid">
		<?php foreach ($data['characteristics'] as $characteristic) : ?>
			<option value="<?php echo($characteristic['characteristicID']); ?>"><?php echo($characteristic['characteristic']); ?></option>
		<?php endforeach; ?>
		</select>

		<!-- Add Tag Button -->
		<input type="button" value="Save Tag" id="save-new-tag" class="pure-button pure-button-primary">
	</div>





	<!-- TAG TABLE -->
	<div class="pure-control-group">
		<label style="float:left;">Assigned Tags</label>
		
		<table class="pure-table pure-table-horizontal" style="padding-left:0px; width:500px;font-size:.875em;">
				<thead>
					<td>Tag Name</td>
					<td>Characteristic</td>
					<td>Remove</td>
				</thead>
				<tbody id="tag-table">
					
					<?php foreach ($data['commentTags'] as $tag) : ?>
						<?php if($tag['hidden'] == 0) : ?>
					<tr class="tag-row" tagid="<?php echo($tag['tagID']); ?>" characteristicid="<?php echo($tag['characteristicID']); ?>">
						<td><?php echo($tag['tagName']); ?></td>
						<td><?php echo($tag['characteristic']); ?></td>
						<td><input type="button" value="Remove" class="pure-button remove-tag"></td>
					</tr>
						<?php endif; ?>
					<?php endforeach; ?>

					<tr id="no-tags-message" class="<?php echo(count($data['commentTags']) > 0 ? 'hidden' : ''); ?>">
						<td colspan="3" style="text-align:center">There are no tags attached to this comment.</td>
					</tr>
				</tbody>
		</table>
	</div>
	

	<br />

	<!-- Submit Tagging -->
	<div class="pure-control-group">
		<label></label>
		<?php if($commentID == -1) : ?>
		<input type="button" id="FormSubmit" value="Submit" class="pure-button pure-button-primary">
		<?php else : ?>
			<input type="button" id="remove-all-tags" name="remove-all-tags" value="Remove All" class="pure-button pure-button-primary">
			<input type="submit" id="nextuntagged" name="nextuntagged" value="Next Untagged" class="pure-button pure-button-primary">
			<input type="submit" id="deletecomment" name="deletecomment" value="Delete Comment" class="pure-button pure-button-primary"
				onclick="return confirm('This will permanently delete the comment and remove any tags that have been attached to it.\n\nAre you sure you want to delete it?', 'yes');">
		<?php endif; ?>
	</div>
</fieldset>
</form>



