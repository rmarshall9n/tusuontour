<?php
/**
 * Sample layout
 */

use Core\Language;

?>
<h2 class="title"><?php echo($data['heading']); ?></h2>

<form action="tagcomment" method="POST" class="pure-form pure-form-aligned">
<fieldset>

    <div class="pure-control-group">
        <label for="studentNo">Student Number</label>
        <input id="studentNo" name="studentNo" type="text" placeholder="Student Number">
    </div>

    <div class="pure-control-group">
        <label for="comment">Comment</label>
        <textarea id="comment" name="comment" placeholder="Comment" style="width:500px; height:300px" required></textarea>
    </div>


    <div class="pure-control-group">
        <label for="Location">Set Location</label>
        
        <select id="Location" name="locationID">

            <?php foreach ($data['locations'] as $location) : ?>
            <option value="<?php echo($location['locationID']); ?>" <?php echo($data['previousLocationID'] == $location['locationID'] ? 'selected' : ''); ?>><?php echo($location['locationName']); ?></option>
            <?php endforeach; ?>

        </select>
    </div>

    <div class="pure-control-group">
        <label></label>
        <input type="submit" value="Save and Tag" name="addcomment" class="pure-button pure-button-primary">
    </div>

</fieldset>
</form>