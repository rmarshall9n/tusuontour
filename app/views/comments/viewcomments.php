<h2 class="title"><?php echo $data['heading']; ?></h2>

<table class="pure-table pure-table-horizontal datatables-full">

	<thead>
		<td>Time</td>
		<td>Student Number</td>
		<td>Comment</td>
		<td>Location</td>
		<td>Options</td>
	</thead>

	<tbody>
		<?php foreach($data['comments'] as $comment): ?>
		<tr>
			<td><?php echo( date('d/m/Y', $comment['timestamp'])); ?></td>
			<td><?php echo($comment['studentNumber']); ?></td>
			<td><?php echo($comment['comment']); ?></td>
			<td><?php echo($comment['locationName']); ?></td>
			<td>
				<form action="tagcomment" method="GET">
					<input type="hidden" name="commentID" value="<?php echo($comment['commentID']); ?>">
					<input type="submit" value="View" class="pure-button pure-button-primary" style="width:70px;">
				</form>
				<form action="" method="POST">
					<input type="hidden" name="commentid" value="<?php echo($comment['commentID']); ?>">
					<input type="submit" value="Delete" name="deletecomment" class="pure-button pure-button-primary" style="width:70px; margin-top:3px;" 
						onclick="return confirm('This will permanently delete the comment and remove any tags that have been attached to it.\n\nAre you sure you want to delete it?', 'yes');">
				</form>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

