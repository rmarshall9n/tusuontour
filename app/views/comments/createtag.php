<!-- ADD NEW TAG FORM -->
<form method="POST" class="pure-form pure-form-aligned">

<legend>Create a new tag</legend>
<fieldset>

	<input type="hidden" name="commentID" value="<?php echo $data['commentID']; ?>">

	<!-- New Tag Name -->
	<div class="pure-control-group">
		<label for="tagname">Tag Name</label>
		<input id="tagname" name="tagname" type="text" placeholder="Tag Name" required>
	</div>

	<div class="pure-control-group">
		<label for="parentid">Parent Tag</label>
		<select id="parentid" name="parentid">
			<option value="-1">None</option>
			<?php foreach ($data['tagList'] as $tagHeading) : ?>
			<option value="<?php echo($tagHeading['tagID']); ?>"><?php echo($tagHeading['tagName']); ?></option>
			<?php endforeach; ?>
		</select>
	</div>

	<div class="pure-control-group">
		<label></label>
		<input type="submit" name="createtag" value="Create" class="pure-button pure-button-primary">
	</div>
	
</fieldset>
</form>
