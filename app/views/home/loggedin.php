<?php

use Core\Language;

?>

<h2 class="title"><?php echo $data['heading']; ?></h2>
<p>
<b>*Student Data Entry*</b><br>
This form is to be used by students to enter comments into the system.
After entering their comments and student number, they will be referenced
with SITS to get extra data and flagged for needing tags.
</p>

<p>
<b>*Staff Data Entry*</b><br>
This form is to be used by staff. They will be able to enter comments written
by students and tag them on the fly.
</p>

<p>
<b>*Comments*</b><br>
A list of all student comments will be displayed here. Users can filter it by
multiple tags such as category, characteristic and severity etc.
</p>

<p>
<b>*Untagged Comments*</b><br>
A list of all comments that have not been tagged as of yet. Comments can be
opened for tagging here.
</p>
<br>