<?php
/**
 * Sample layout
 */

use Core\Language;

?>

<form action="" method="POST" class="pure-form pure-form-aligned">
<fieldset>

    <div class="pure-control-group">
        <label for="studentNo">Student Number</label>
        <input id="studentNo" name="studentNo" type="text" placeholder="Student Number">
    </div>

    <div class="pure-control-group">
        <label for="comment">Comment</label>
        <textarea id="comment" name="comment" placeholder="Comment" style="width:500px; height:300px" required></textarea>
    </div>

    <div class="pure-control-group">
        <label></label>
        <input type="submit" value="Submit" name="submit" class="pure-button pure-button-primary">
    </div>


    <div class="pure-control-group">
        <label for="Location">Set Location</label>
        
        <select id="Location" name="locationID">

            <?php foreach ($data['locations'] as $location) : ?>
            <option value="<?php echo($location['locationID']); ?>" <?php echo($data['previousLocationID'] == $location['locationID'] ? 'selected' : ''); ?>><?php echo($location['locationName']); ?></option>
            <?php endforeach; ?>

        </select>
    </div>

</fieldset>
</form>

<!-- login form box -->
<input type="button" name="adminLogin" value="Admin Log in" class="pure-button" onclick="$('#AdminLoginFrom').slideToggle();"/>

<form method="POST" id="AdminLoginFrom" action="" name="loginform" class="pure-form" style="display:none;">
    <input type="text" name="username" placeholder="Username" required />
    <input type="password" name="password" autocomplete="off" placeholder="Password" required />
    <input type="submit" name="login" value="Log in" class="pure-button pure-button-primary" />
    
</form>
