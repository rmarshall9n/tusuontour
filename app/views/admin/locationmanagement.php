<?php
/**
 * Sample layout
 */

use Core\Language;

?>

<h2 class="title"><?php echo($data['heading']); ?></h2>

<!-- CREATE LOCATION -->
<form method="POST" class="pure-form pure-form-aligned">

<legend>Create a new location</legend>
<fieldset>

	<!-- New Tag Name -->
	<div class="pure-control-group">
		<label for="locationname">Tag Name</label>
		<input id="locationname" name="locationname" type="text" placeholder="Location Name" style="width:250px;margin-right:10px;" required> 
		<input type="submit" name="createlocation" value="Create" class="pure-button pure-button-primary">
	</div>

</fieldset>
</form>



<!-- LOCATION LIST TABLE -->

<legend>Create a new location</legend>

<table class="pure-table pure-table-horizontal datatables-minimum">
	<thead>
		<td>Tag</td>
		<td>Options</td>
	</thead>
	<tbody>

		<?php foreach($data['locationList'] as $location): ?>
		<tr class="locationrow" locationid="<?php echo($location['locationID']); ?>">
			<td><?php echo($location['locationName']); ?></td>
			<td>
				<input type="button" value="Delete" name="deletelocation" class="pure-button pure-button-primary deletelocation">
			</td>
		</tr>

		<?php endforeach; // location list ?>
	</tbody>
</table>
