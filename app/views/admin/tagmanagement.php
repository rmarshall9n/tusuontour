<?php
/**
 * Sample layout
 */

use Core\Language;

?>

<!-- TAG LIST TABLE -->
<table class="pure-table pure-table-horizontal datatables-minimum" style="min-width: 60%">
	<thead>
		<td>Tag</td>
		<td>Options</td>
	</thead>
	<tbody>

		<?php foreach($data['tagList'] as $tagHeading): ?>
		<tr class="tagrow" tagid="<?php echo($tagHeading['tagID']); ?>" style="height:20px;">
			<td style="font-weight:bold;"><?php echo($tagHeading['tagName']); ?></td>
			<td>
			<?php if(count($tagHeading['subTags']) <= 0) : ?>	
				<input type="button" value="Delete" name="deleteTag" class="pure-button deletetag">
			<?php endif; ?>
			</td>
		</tr>

		<?php foreach($tagHeading['subTags'] as $subTag): ?>
		<tr class="tagrow" tagid="<?php echo($subTag['tagID']); ?>" style="height:20px;">
			<td style="padding-left: 50px;"><?php echo($subTag['tagName']); ?></td>
			<td>
			<?php if($subTag['tagID'] != 0) : ?>
				<input type="button" value="Delete" name="deleteTag" class="pure-button deletetag">
			<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; // sub tag ?>
		<?php endforeach; // tag heading ?> 
	</tbody>
</table>
