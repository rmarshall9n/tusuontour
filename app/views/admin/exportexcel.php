<?php

// create output
$objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');

// initiate download
ob_end_clean();
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=\"tusuontourexport.xlsx\"");
header("Cache-Control: max-age=0");
$objWriter->save("php://output");
die();

// save localy
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__)); 

?>