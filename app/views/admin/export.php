<p>Click the export button to generate a copy of all TUSU on Tour data in a Excel 2007 file.</p>

<form method="POST" action="exportexcel" class="pure-form pure-form-aligned" target="_blank">
	<div class="pure-controls">
		<input type="submit" name="exportTagCount" value="Export" class="pure-button pure-button-primary">
	</div>
</form>
<br />


<h3 class="title">Export Date Range</h3>
<hr/>
<p>Select a start and end date then click the export button. This will generate a copy of all TUSU on Tour data in a Excel 2007 file.</p>
<form method="POST" action="exportdownload" class="pure-form pure-form-aligned" target="_blank">

	<div class="pure-control-group">
		<label for="StartDate">Start Date</label>
		<input type="date" id="StartDate" name="startDate">
	</div>
	
	<div class="pure-control-group">
		<label for="EndDate">End Date</label>
		<input type="date" id="EndDate" name="endDate">
	</div>
	
	<div class="pure-controls">
		<input type="submit" name="exportTagCount" value="Export" class="pure-button pure-button-primary">
	</div>
</form>