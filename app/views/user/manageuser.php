<?php
/**
 * Sample layout
 */

use Core\Language;

?>
<h2 class="title"><?php echo($data['heading']); ?></h2>
<h3 class="title">Update Password</h3>
<form action="manageuser" method="POST" class="pure-form pure-form-aligned">
<fieldset>

	<div class="pure-control-group">
		<label for="oldPassword">Current Password</label>
		<input id="oldPassword" name="oldPassword" type="password" placeholder="Current Password" required>
	</div>

	<div class="pure-control-group">
		<label for="newpassword">New Password</label>
		<input id="newpassword" name="newpassword" type="password" placeholder="New Password" required>
	</div>

	<div class="pure-control-group">
		<label for="passwordrepeat">Repeat Password</label>
		<input id="passwordrepeat" name="passwordrepeat" type="password" placeholder="Password" required>
	</div>

	<div class="pure-control-group">
		<label></label>
		<input type="submit" value="Update Password" name="updatepassword" class="pure-button pure-button-primary">
	</div>

</fieldset>
</form>