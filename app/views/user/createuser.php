<?php
/**
 * Sample layout
 */

use Core\Language;

?>
<h2 class="title"><?php echo($data['heading']); ?></h2>

<form action="createuser" method="POST" class="pure-form pure-form-aligned">
<fieldset>

    <div class="pure-control-group">
        <label for="username">Username</label>
        <input id="username" name="username" type="text" placeholder="Username" required>
    </div>

    <div class="pure-control-group">
        <label for="firstname">First name</label>
        <input id="firstname" name="firstname" type="text" placeholder="First Name" required>
    </div>

    <div class="pure-control-group">
        <label for="surname">Surname</label>
        <input id="surname" name="surname" type="text" placeholder="Surname" required>
    </div>
    
    <div class="pure-control-group">
        <label for="email">Email</label>
        <input id="email" name="email" type="email" placeholder="Email" required>
    </div>

    <div class="pure-control-group">
        <label for="password">Password</label>
        <input id="password" name="password" type="password" placeholder="Password" required>
    </div>

    <div class="pure-control-group">
        <label for="passwordrepeat">Repeat Password</label>
        <input id="passwordrepeat" name="passwordrepeat" type="password" placeholder="Password" required>
    </div>

    <div class="pure-control-group">
        <label></label>
        <input type="submit" value="createuser" name="createuser" class="pure-button pure-button-primary">
    </div>

</fieldset>
</form>