<?php
/**
 * Admin controller
 *
 * @author  David Carr - dave@daveismyname.com
 * @version 2.2
 * @date    June 27, 2014
 * @date    updated Sept 19, 2015
 */

namespace Controllers;

use Core\View;
use Core\Controller;
use PHPExcel;

class Admin extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->language->load('Admin');

		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}
	}


	public function tagManagement()
	{
		$objPHPExcel = new PHPExcel();

		// Set up Models
		$tags = new \Models\Tags();
		

		if (isset($_REQUEST['createtag'])) {
			// create a new tag
			$tagID = $tags->createTag($_REQUEST["tagname"], $_REQUEST["parentid"]);
			\Helpers\Url::redirect('tagmanagement');
		}

		if (isset($_REQUEST['deletetag'])) {
			// create a new tag
			$tags->deleteTag($_REQUEST["tagID"]);
		}

		// CREATE PAGE DATA
		$templateData['title'] = "Admin";
		$templateData['javascript'] = array("tagmanagement");
		$templateData['heading'] = "Tag Management";

		$data['tagList'] = $tags->getTags();
		
		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('comments/createtag', $data);
		View::render('admin/tagmanagement', $data);
		View::renderTemplate('footer', $templateData);
	}


	public function locationManagement()
	{
		// Set up Models
		$comments = new \Models\Comments();

		if (isset($_REQUEST['createlocation'])) {
			// create a new tag
			$locationID = $comments->createLocation($_REQUEST["locationname"]);
			\Helpers\Url::redirect('locationmanagement');
		}

		if (isset($_REQUEST['deletelocation'])) {
			// create a new tag
			$comments->deleteLocation($_REQUEST["locationID"]);
		}

		// CREATE PAGE DATA
		$templateData['title'] = "Admin";
		$templateData['javascript'] = array("locationmanagement");
		$templateData['heading'] = "Location Management";

		$data['locationList'] = $comments->getLocations();
		

		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('admin/locationmanagement', $data);
		View::renderTemplate('footer', $templateData);
	}

	public function export()
	{

		// Set up Models
		//$comments = new \Models\Comments();

		// CREATE PAGE DATA
		$templateData['title'] = "Admin";
		$templateData['javascript'] = array();
		$templateData['heading'] = "Export";

		
		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('admin/export', $data);
		View::renderTemplate('footer', $templateData);
	}
}
