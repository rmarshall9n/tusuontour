<?php

namespace Controllers;

use Core\View;
use Core\Controller;
use PHPExcel;

define('EOL', (PHP_SAPI== 'cli') ? PHP_EOL : '<br />');

class Export extends Controller
{

	private $objPHPExcel;
	private $startDate;
	private $endDate;
	private $comments;
	private $tags;

	public function __construct()
	{
		parent::__construct();
		$this->language->load('Admin');

		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}

		$this->objPHPExcel = new PHPExcel();
		$this->comments = new \Models\Comments();
		$this->tags = new \Models\Tags();
	}


	// export to Excel 2007
	public function exportExcel()
	{
		if (isset($_REQUEST['exportexcel'])) {
			$this->startDate = $_REQUEST['startDate'];
			$this->endDate = $_REQUEST['endDate'];
		}

		// Set document properties
		$this->objPHPExcel->getProperties()->setCreator("TUSU on Tour")
			->setLastModifiedBy("TUSU on Tour")
			->setTitle("TUSU on Tour")
			->setSubject("TUSU on Tour")
			->setDescription("Date export for TUSU on Tour.")
			->setKeywords("TUSU on Tour export")
			->setCategory("TUSU on Tour");

		// Create pages
		self::createTagsPage();
		self::createTagCountPage();
		self::createCommentsPage();
		
		self::downloadExcel2007("tusuontour");
	}

	private function downloadExcel2007($filename)
	{
		// create output
		$objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		
		// initiate download
		ob_end_clean();
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=\"".$filename.".xlsx\"");
		header("Cache-Control: max-age=0");
		$objWriter->save("php://output");
		die();

		// save localy
		//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
	}

	private function createCommentsPage()
	{
		$commentList = $this->comments->getComments();// start date end date;

		// Create sheet
		$myWorkSheet = new \PHPExcel_Worksheet($this->objPHPExcel, 'Comments');
		$this->objPHPExcel->addSheet($myWorkSheet, 0);
		
		// Add headings
		$this->objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'ID')
			->setCellValue('B1', 'Date')
			->setCellValue('C1', 'Student Number')
			->setCellValue('D1', 'Location')
			->setCellValue('E1', 'Comment');

		//Populate with data
		$rowNum = 2;
		for ($i=0; $i < count($commentList); $i++) {
			$this->objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$rowNum, $commentList[$i]['commentID'])
				->setCellValue('B'.$rowNum, date('d/m/Y', $commentList[$i]['timestamp']))
				->setCellValue('C'.$rowNum, $commentList[$i]['studentNumber'])
				->setCellValue('D'.$rowNum, $commentList[$i]['locationName'])
				->setCellValue('E'.$rowNum, $commentList[$i]['comment']);

			$rowNum++;
		}
	}

	private function createTagCountPage()
	{
		$tagCounts = $this->tags->getTagCharacteristicCounts();
		
		// Create sheet
		$myWorkSheet = new \PHPExcel_Worksheet($this->objPHPExcel, 'Tag Count');
		$this->objPHPExcel->addSheet($myWorkSheet, 0);
		
		// Add headings
		$this->objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Tag Name')
			->setCellValue('B1', 'Tag Parent')
			->setCellValue('C1', 'Total Count')
			->setCellValue('D1', 'Positive Count')
			->setCellValue('E1', 'Negative Count')
			->setCellValue('F1', 'Neutral Count');

		//Populate with data
		$rowNum = 2;
		for ($i=0; $i < count($tagCounts); $i++) {
			// default parent name to this tags name
			$parentTagName = $tagCounts[$i]['tagName'];

			// if the tag has a parent, use its parents name.
			if ($tagCounts[$i]['parentID'] > 0) {
				$tagParent = $this->tags->getTagByID($tagCounts[$i]['parentID']);
				$parentTagName = $tagParent['tagName'];
			}

			$this->objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$rowNum, $tagCounts[$i]['tagName'])
				->setCellValue('B'.$rowNum, $parentTagName)
				->setCellValue('C'.$rowNum, $tagCounts[$i]['total'])
				->setCellValue('D'.$rowNum, $tagCounts[$i]['positiveCount'])
				->setCellValue('E'.$rowNum, $tagCounts[$i]['negativeCount'])
				->setCellValue('F'.$rowNum, $tagCounts[$i]['neutralCount']);

			$rowNum++;
		}
	}

	private function createTagsPage()
	{

		$tagDetails = $this->tags->getAllTagsForComments();

		// Create sheet
		$myWorkSheet = new \PHPExcel_Worksheet($this->objPHPExcel, 'Tags');
		$this->objPHPExcel->addSheet($myWorkSheet, 0);
		
		// Add headings
		$this->objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Comment ID')
			->setCellValue('B1', 'Tag ID')
			->setCellValue('C1', 'Characteristic')
			->setCellValue('D1', 'Location')
			->setCellValue('E1', 'Date');

		//Populate with data
		for ($i=0; $i < count($tagDetails); $i++) {
			$rowNum = $i+2;
			$this->objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$rowNum, $tagDetails[$i]['commentID'])
				->setCellValue('B'.$rowNum, $tagDetails[$i]['tagName'])
				->setCellValue('C'.$rowNum, $tagDetails[$i]['characteristic'])
				->setCellValue('D'.$rowNum, $tagDetails[$i]['locationName'])
				->setCellValue('E'.$rowNum, date('d/m/Y', $tagDetails[$i]['timestamp']));
		}
	}
}
