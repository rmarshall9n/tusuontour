<?php
/**
 * User controller
 */

namespace Controllers;

use Core\View;
use Core\Controller;

class User extends Controller
{

	/**
	 * Call the parent construct
	 */
	public function __construct()
	{
		parent::__construct();
		$this->language->load('User');

		$templateData['title'] = "".$_SESSION['username'];
	}

	
	public function logout()
	{
		$login = new \Models\Login();
		$login->logout();
		\Helpers\Url::redirect('');
	}


	public function createUser()
	{
		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}
		
		// Handle Submit click
		if (isset($_POST['createuser'])) {
			$newUser = $user->createUser(
				$_SESSION['userID'],
				$_POST['username'],
				$_POST['password'],
				$_POST['passwordrepeat'],
				$_POST['firstname'],
				$_POST['surname'],
				$_POST['email']
			);
		}

		// CREATE PAGE DATA
		$templateData['heading'] = "Create User";
		
		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('user/createuser', $data);
		View::renderTemplate('footer', $templateData);
	}


	public function manageUser()
	{
		// redirect if not logged in
		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}
		
		// Handle Submit click
		if (isset($_POST['updatepassword'])) {
			$newUser = $user->updatePassword(
				$_SESSION['userID'],
				$_POST['oldPassword'],
				$_POST['newpassword'],
				$_POST['passwordrepeat']
			);
		}

		// CREATE PAGE DATA
		$templateData['heading'] = "Manage User";
		
		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('user/manageuser', $data);
		View::renderTemplate('footer', $templateData);
	}
}
