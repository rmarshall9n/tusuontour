<?php
/**
 * Comments controller
 *
 * @author  David Carr - dave@daveismyname.com
 * @version 2.2
 * @date    June 27, 2014
 * @date    updated Sept 19, 2015
 */

namespace Controllers;

use Core\View;
use Core\Controller;

// use Models\Comments;
// use Models\Tags;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class Comments extends Controller
{

	/**
	 * Call the parent construct
	 */
	public function __construct()
	{
		parent::__construct();
		$this->language->load('Comments');

		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}
	}


	public function allComments()
	{
		// Set up Models
		$comments = new \Models\Comments();
		
		if (isset($_POST['deletecomment']) && isset($_POST['commentid'])) {
			$comments->deleteComment($_POST['commentid']);
		}

		// CREATE PAGE DATA
		$templateData['title'] = "Comments";
		$data['heading'] = "All Comments";
		$data['comments'] = $comments->getComments();
		

		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('comments/viewcomments', $data);
		View::renderTemplate('footer', $templateData);
	}

	public function untaggedComments()
	{
		// Set up Models
		$comments = new \Models\Comments();
		
		if (isset($_POST['deletecomment']) && isset($_POST['commentid'])) {
			$comments->deleteComment($_POST['commentid']);
		}

		// CREATE PAGE DATA
		$templateData['title'] = "Comments";
		$data['heading'] = "Untagged Comments";
		$data['comments'] = $comments->getUntaggedComments();		

		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('comments/viewcomments', $data);
		View::renderTemplate('footer', $templateData);
	}

	public function tagComment()
	{
		$comments = new \Models\Comments();
		$tags = new \Models\Tags();
		
		$commentID = -1;
		// See if a valid commentID has been passed through request
		if (isset($_REQUEST['commentID']) && is_numeric($_REQUEST['commentID']) && $_REQUEST['commentID'] != -1) {
			$commentID = $_REQUEST["commentID"];
		}

		if (isset($_POST['deletecomment']) && $commentID >= 0) {
			$comments->deleteComment($commentID);
			\Helpers\Url::redirect('allcomments');
		}

		// If we are adding a new comment
		if (isset($_REQUEST['addcomment'])) {
			// Add the comment
			$commentID = $comments->addComment($_REQUEST["studentNo"], $_REQUEST["comment"], $_REQUEST["locationID"]);
		} elseif (isset($_REQUEST['createtag'])) {
			// create a new tag
			$tagID = $tags->createTag($_REQUEST["tagname"], $_REQUEST["parentid"]);
		} elseif ($_REQUEST['nextuntagged']) {
			$commentID = $comments->getNextUntaggedComment($_REQUEST['commentID']);
			\Helpers\Url::redirect('tagcomment?commentID='.$commentID);
		}

		if ($comments->getComment($commentID) != -1) {
			$this->viewTagComment($commentID);
		} else {
			$this->viewAddComment();
		}
	}

	public function viewTagComment($commentID)
	{
		$comments = new \Models\Comments();
		$tags = new \Models\Tags();

		// CREATE PAGE DATA
		$templateData['title'] = "Comments";
		$templateData['javascript'] = array("tagcomment");
		$data['heading'] = "Tag Comment";
		$data['locations'] = $comments->getLocations();
		$data['characteristics'] = $tags->getCharacteristics();
		$data['commentTags'] = $tags->getTagsByCommentID($commentID);
		$data['tagList'] = $tags->getTags();
		$data['commentInfo'] = $comments->getComment($commentID);
		
		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('comments/tagcomment', $data);
		View::render('comments/createtag', $data);
		View::renderTemplate('footer', $templateData);
	}

	public function viewAddComment()
	{
		// Set up Models
		$locations = new \Models\Comments();

		// CREATE PAGE DATA
		$templateData['title'] = "Comments";
		$data['heading'] = "Add Comment";
		$data['locations'] = $locations->getLocations();
		$data['previousLocationID'] = $previousLocationID;

		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('comments/addcomment', $data);
		View::renderTemplate('footer', $templateData);
	}
}
