<?php

namespace Controllers;

use Core\View;
use Core\Controller;

class Home extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->language->load('Home');
	}

	public function home()
	{
		$user = new \Models\User();

		// first check if were trying to log in
		if (isset($_POST['login'])) {
			$user->attemptLogin($_POST['username'], $_POST['password']);
		}

		// go to the correct page if logged in or not
		if ($user->isUserLoggedIn()) {
			self::loggedIn();
		} else {
			self::loggedOut();
		}
	}

	public function logout()
	{
		$user = new \Models\User();
		$user->logout();
		\Helpers\Url::redirect('');
	}


	public function loggedOut()
	{
		// Set up Models
		$comments = new \Models\Comments();
		
		// Handle Submit click
		if (isset($_POST['submit'])) {
			// validate input
			$is_valid = \Helpers\Gump::is_valid(
				$_POST,
				array(
				'comment' => 'required',
				'locationID' => 'required'
				)
			);

			// Add the comment
			if ($is_valid === true) {
				$newCommentID = $comments->addComment($_POST["studentNo"], $_POST["comment"], $_POST["locationID"]);
			}

			// set previous location
			$previousLocationID = $_POST['locationID'];
		}

		// CREATE PAGE DATA
		$templateData['title'] = "";
		$data['locations'] = $comments->getLocations();
		$data['previousLocationID'] = $previousLocationID;


		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::render('home/loggedout', $data);
		View::renderTemplate('footer', $templateData);
	}

	public function loggedIn()
	{
		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}
		
		$comments = new \Models\Comments();

		$templateData['title'] = "Home";
		$data['heading'] = "Home";

		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('home/loggedin', $data);
		View::renderTemplate('footer', $templateData);
	}
}
