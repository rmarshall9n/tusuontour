<?php

namespace Controllers;

use Core\View;
use Core\Controller;

class Analysis extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->language->load('Analysis');

		// check if logged in
		$user = new \Models\User();
		if (!$user->isUserLoggedIn()) {
			\Helpers\Url::redirect('');
		}
	}


	public function tagOverview()
	{
		// Set up Models
		$analysis = new \Models\Analysis();

		$topTotals = $analysis->getTopTags(6);
		$topTotals = self::createDataArray($topTotals, 'total', 'tagName');

		$topPositive = $analysis->getTopTags(4, 1);
		$topPositive = self::createDataArray($topPositive, 'total', 'tagName');

		$topNegative = $analysis->getTopTags(4, 2);
		$topNegative = self::createDataArray($topNegative, 'total', 'tagName');

		// chart1 data
		$chart1 = array(
			'chartID' => "chart1",
			'chartType' => "bar",
			'data' => $topTotals['data'],
			'labels' => $topTotals['labels']
			);

		$chart2 = array(
			'chartID' => "chart2",
			'chartType' => "bar",
			'data' => $topPositive['data'],
			'labels' => $topPositive['labels']
			);

		$chart3 = array(
			'chartID' => "chart3",
			'chartType' => "bar",
			'data' => $topNegative['data'],
			'labels' => $topNegative['labels']
			);

		$chartDisplay = array(
			array('chartID' => 'chart1', 'title' => "Top 6 tags added to comments", 'span' => 2),
			array('chartID' => 'chart2', 'title' => "Top 4 positive tags added to comments",'span' => 1),
			array('chartID' => 'chart3', 'title' => "Top 4 negative tags added to comments",'span' => 1),
			);

		
		// CREATE PAGE DATA
		$templateData['title'] = "Analysis";
		$templateData['javascript'] = array("chart", "dynamiccharts");
		$data['heading'] = "Tag Overview";
		$chartData['chartDisplay'] = $chartDisplay;
		$chartData['chartData'] = array($chart1, $chart2, $chart3);

		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('analysis/tagoverview', $chartData);
		View::render('analysis/dynamiccharts', $chartData);
		View::renderTemplate('footer', $templateData);
	}


	

	public function tagAnalysis()
	{
		// Set up Models
		$analysis = new \Models\Analysis();
		$comments = new \Models\Comments();
		$tags = new \Models\Tags();

		$tagID = -1;
		if (isset($_GET['tagid'])) {
			$tagID = $_GET['tagid'];

			$numMonths = 12;
			$tagMonthlyCount = $analysis->getTagsPerMonth($numMonths, $tagID);

			$totalPerMonth = self::createDataArray($tagMonthlyCount, 'total', 'month');
			$positivesPerMonth = self::createDataArray($tagMonthlyCount, 'positiveCount', 'month');
			$negativesPerMonth = self::createDataArray($tagMonthlyCount, 'negativeCount', 'month');

			// chart1 data
			$chart1 = array(
				'chartID' => "chart1",
				'chartType' => "bar",
				'data' => $totalPerMonth['data'],
				'labels' => $totalPerMonth['labels']
				);

			$chart2 = array(
				'chartID' => "chart2",
				'chartType' => "bar",
				'data' => $positivesPerMonth['data'],
				'labels' => $positivesPerMonth['labels']
				);

			$chart3 = array(
				'chartID' => "chart3",
				'chartType' => "bar",
				'data' => $negativesPerMonth['data'],
				'labels' => $negativesPerMonth['labels']
				);

			$chartDisplay = array(
				array('chartID' => 'chart1', 'title' => "Total tags over gathered over the past $numMonths months.", 'span' => 2),
				array('chartID' => 'chart2', 'title' => "Positive tags over gathered over the past $numMonths months.",'span' => 1),
				array('chartID' => 'chart3', 'title' => "Negative tags over gathered over the past $numMonths months.",'span' => 1),
				);


			$templateData['javascript'] = array("chart", "dynamiccharts", "jquery-ui");
			$data['tagCountThisMonth'] = $tagMonthlyCount[0];
			$data['tagInfo'] = $analysis->getTagInfo($tagID);
			$data['tagComments'] = $comments->getCommentsWithTagID($tagID);
			$data['chartData']['chartDisplay'] = $chartDisplay;
			$data['chartData']['chartData'] = array($chart1, $chart2, $chart3);
		}

		// CREATE PAGE DATA
		$templateData['title'] = "Analysis";
		$templateData['heading'] = "Tag Analysis";
		$data['tagList'] = $tags->getTags();
		$data['selectedTag'] = $tags->getTagByID($tagID);
		
		// RENDER PAGE
		View::renderTemplate('header', $templateData);
		View::renderTemplate('navigation', $templateData);
		View::render('analysis/taganalysis', $data);
		View::renderTemplate('footer', $templateData);
	}




	




	private function getPastXMonths($numMonths)
	{

		$months = array();
		for ($i = 1; $i <= $numMonths; $i++) {
			$months[] = date("Y-m%", strtotime(date('Y-m-01')." -$i months"));
		}
		return $months;
	}

	private function createDataArray($rawData, $dataName, $labelName)
	{

		$dataArray = array();
		$labelArray = array();
		foreach ($rawData as $dataItem) {
			$dataArray[] = $dataItem[$dataName];
			$labelArray[] = $dataItem[$labelName];
		}

		return array('data' => $dataArray, 'labels' => $labelArray);
	}
}
