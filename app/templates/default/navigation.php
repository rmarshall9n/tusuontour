
<center>

<div class="pure-menu pure-menu-horizontal">
    <ul class="pure-menu-list">
        <li class="pure-menu-item"><a href="home" class="pure-menu-link">Home</a></li>
        <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
            <a href="tagcomment" id="menuLink1" class="pure-menu-link">Comments</a>
            <ul class="pure-menu-children">
                <li class="pure-menu-item"><a href="tagcomment" class="pure-menu-link">Add &amp; Tag</a></li>
                <li class="pure-menu-item"><a href="allcomments" class="pure-menu-link">View All</a></li>
                <li class="pure-menu-item"><a href="untaggedcomments" class="pure-menu-link">View Untagged</a></li>
            </ul>
        </li>
        
        <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
            <a href="tagoverview" id="menuLink2" class="pure-menu-link">Analysis</a>
            <ul class="pure-menu-children">
                <li class="pure-menu-item"><a href="tagoverview" class="pure-menu-link">Tag Overview</a></li>
                <li class="pure-menu-item"><a href="taganalysis" class="pure-menu-link">Tag Analysis</a></li>
            </ul>
        </li>
        
        <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
            <a href="#" id="menuLink3" class="pure-menu-link">Admin</a>
            <ul class="pure-menu-children">
                <li class="pure-menu-item"><a href="export" class="pure-menu-link">Export</a></li>
                <li class="pure-menu-item"><a href="locationmanagement" class="pure-menu-link">Location Management</a></li>
                <li class="pure-menu-item"><a href="tagmanagement" class="pure-menu-link">Tag Management</a></li>
            </ul>
        </li>


        <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
            <a href="manageuser" id="menuLink4" class="pure-menu-link" style="text-transform: capitalize"><?php echo($_SESSION['username']); ?></a>
            <ul class="pure-menu-children">
                <li class="pure-menu-item"><a href="createuser" class="pure-menu-link">Create User</a></li>
                <li class="pure-menu-item"><a href="manageuser" class="pure-menu-link">Manage User</a></li>
                <li class="pure-menu-item"><a href="logout" class="pure-menu-link">Logout</a></li>
            </ul>
        </li>

    </ul>
</div>

</center>
