<?php
/**
 * Sample layout
 */

use Helpers\Assets;
use Helpers\Url;
use Helpers\Hooks;


//initialise hooks
$hooks = Hooks::get();
?>


<br />
<br />
<center>
	Copyright &copy; <?php echo(date("Y")); ?> Teesside University Students' Union | Ryan Marshall
</center>
<br />
</div>

<!-- JS -->
<?php

$javascriptFiles = array(
	Url::templatePath() . 'js/jquery.js',
	Url::templatePath() . 'js/jquery-ui.js',
	Url::templatePath() . 'js/jquery.datatables.js',
	Url::templatePath() . 'js/initplugins.js'
);

if (isset($data['javascript'])) {
    foreach ($data['javascript'] as $jsFile) {
        array_push($javascriptFiles, Url::templatePath() . "js/" . $jsFile . ".js");
    }
}

Assets::js($javascriptFiles);

//hook for plugging in javascript
$hooks->run('js');

//hook for plugging in code into the footer
$hooks->run('footer');
?>


</body>
</html>
