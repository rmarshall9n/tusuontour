<?php
/**
 * Sample layout
 */

use Helpers\Assets;
use Helpers\Url;
use Helpers\Hooks;

//initialise hooks
$hooks = Hooks::get();
?>
<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
<head>

	<!-- Site meta -->
	<meta charset="utf-8">
	<?php
	//hook for plugging in meta tags
	$hooks->run('meta');
	?>
	<title><?php echo SITETITLE.' - '.$data['title'].' - '.SITEVERSION; ?></title>

	<!-- CSS -->
	<?php
	Assets::css(array(
		'//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css',
		Url::templatePath() . 'css/pure-min.css',
		Url::templatePath() . 'css/jquery-ui.css',
		Url::templatePath() . 'css/style.css',
		Url::templatePath() . 'css/jquery.datatables.css'
	));

	//hook for plugging in css
	$hooks->run('css');
	?>
	
	<link rel="icon" type="image/png" href="<?php echo Url::templatePath().SITEFAVICON ?>">

</head>
<body>

<?php
//hook for running code after body tag
$hooks->run('afterBody');
?>

<div class="container">
<center style="padding-top:10px; padding-bottom:10px;">
	<img style="display:inline-block;" src="<?php echo Url::templatePath().SITELOGO ?>">
    <h1 class="title" style="display:inline-block; vertical-align:top;"><?php echo SITETITLE; ?></h1>
</center>