$(document).ready(function(){

	$('.deletelocation').on('click', function(){


		var confirmDelete = confirm("If you delete a location you will remove it form any comments it is currently attached to. \n Are you sure you want to delete it?");
		if(!confirmDelete)
			return;

		var locationRow = $(this).parent().closest('tr');
		var locationID = locationRow.attr('locationid');

		// run ajax to remore tag
		$.ajax({
			method: "GET",
			url: "ajaxdeletelocation",
			data: { locationid: locationID },
		}).done(function (res) {
			if(res.success){

				// remove row
				locationRow.remove();

				// show no rows message if no more locations left in table
				if($('.locationrow').length <= 0) {
					$('#norowsmessage').removeClass('hidden');
				}
			}
			else{
				console.log(res.message);
			}
		});
	});


});