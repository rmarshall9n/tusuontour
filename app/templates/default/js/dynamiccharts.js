$(document).ready(function() {

	var colourTUSUPink = "rgba(222,54,120,0.9)";
	var colourTUSUGrey = "rgba(90,90,90,0.9)";	

	var charts = [];


	Chart.defaults.global.animationEasing = "easeOutElastic";
	
	if( typeof chartData == 'undefined' || chartData.length <= 0)
		return;

	for (var i = 0; i < chartData.length; i++) {
		

		// check data is valid
		if( typeof chartData[i]['data'] == 'undefined' || chartData[i]['data'].length <= 0 || 
			typeof chartData[i]['labels'] == 'undefined' || chartData[i]['labels'].length <= 0) {

			continue;
		}

		var ctx = $("#"+chartData[i]['chartID']).get(0).getContext("2d");
		//$("#recurringNegativeLegend").html(recurringTagChart.generateLegend());


		switch(chartData[i]['chartType']) {

			case "bar":
				var chartDisplay = {
					labels: chartData[i]['labels'],
					datasets: [
						{
							data: chartData[i]['data'],
							fillColor: "rgba(90,90,90,0.2)",
							strokeColor: colourTUSUPink,
							highlightFill: colourTUSUGrey,
							highlightStroke: "rgba(220,220,220,1)"
						}
					]
				};
				charts[i] = new Chart(ctx).Bar(chartDisplay, {labelLength: 10,});
				break;

			case "line":
				var chartDisplay = {
					labels: chartData[i]['labels'],
					datasets: [
						{
							data: chartData[i]['data'],
							fillColor: "rgba(90,90,90,0.2)",
							strokeColor: colourTUSUPink,
							pointColor: colourTUSUPink,
							pointStrokeColor: "#fff",
							pointHighlightFill: colourTUSUGrey,
							pointHighlightStroke: "rgba(220,220,220,1)",
						}
					]
				};
				charts[i] = new Chart(ctx).Line(chartDisplay, {labelLength: 10,});
				break;

			case "pie":
				/*var chartDisplay = [
					{
						value: 300,
						color:"#F7464A",
						highlight: "#FF5A5E",
						label: "Red"
					},
				]

				charts[i] = new Chart(ctx).Pie(chartDisplay);*/
				break;

			case "radar":
				var chartDisplay = {
				    labels: chartData[i]['labels'],
				    datasets: [
				        {
				            data: chartData[i]['data'],
				            fillColor: "rgba(90,90,90,0.2)",
							strokeColor: colourTUSUPink,
							pointColor: colourTUSUPink,
							pointStrokeColor: "#fff",
							pointHighlightFill: colourTUSUGrey,
							pointHighlightStroke: "rgba(220,220,220,1)",
				        }
				    ]
				};

				charts[i] = new Chart(ctx).Radar(chartDisplay);
				break;
		}	
	};
});
	