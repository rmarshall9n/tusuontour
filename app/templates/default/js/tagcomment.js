$(document).ready(function() {


	$('#save-new-tag').on('click', function(){

		// get variables to set tag
		var commentID = $("#comment-id").val();
		var tagID = $("#newtag-tagid option:selected").val();
		var characteristicID = $("#newtag-characteristicid option:selected").val();

		// run ajax
		$.ajax({
			method: "GET",
			url: "ajaxaddtag",
			data: {commentid: commentID, tagid: tagID, characteristicid: characteristicID}
		}).done(function (res) {
			
			if(res.success){

				// if success grab details on added tag
				var tagName = $("#newtag-tagid option:selected").text().trim();
				var characteristic = $("#newtag-characteristicid option:selected").text();
				
				// create html string to add onto the table
				var addHTML = 
					"<tr class='tag-row' tagid='"+tagID+"' characteristic='"+characteristicID+"'>"+
					"<td class='tag'>"+tagName+"</td>"+
					"<td class='characteristic'>"+characteristic+"</td>"+
					"<td><input type='button' value='Remove' class='pure-button remove-tag'></td>"+
					"</tr>";

				//ensure the "no tags" row is hidded
				if(!$('#no-tags-message').hasClass('hidden')) {
					$('#no-tags-message').addClass('hidden');
				}

				// add the new row to the page
				$('#tag-table').prepend(addHTML);
			} 
			else {
				console.log(res.error)
			}
		});
	});


	$('#tag-table').on('click', '.remove-tag', function(){

		// get the current tag row and attributes attached
		var commentID = $("#comment-id").val();
		var tagRow = $(this).parent().closest('tr');
		var tagID = tagRow.attr("tagid");
		var characteristicID = tagRow.attr("characteristicid");

		// run ajax to remore tag
		$.ajax({
			method: "GET",
			url: "ajaxremovetag",
			data: {commentid: commentID, tagid: tagID, characteristicid: characteristicID},
		}).done(function (res) {
			if(res.success){

				// remove row
				tagRow.remove();

				// show no tags row if no more tags left in table
				if($('.tag-row').length <= 0) {
					$('#no-tags-message').removeClass('hidden');
				}
			}
			else{
				console.log(res.message);
			}
		});
	});


	$('#remove-all-tags').on('click', function(){

		// promt the user if they actually want to remove all tags
		if(confirm("Are you sure you want to remove all tags?") == false)
			return;
		
		var commentID = $("#comment-id").val();

		// run ajax to remore tag
		$.ajax({
			method: "GET",
			url: "ajaxremovealltags",
			data: {commentid: commentID}
		}).done(function (res) {

			if(res.success){

				// remove row
				$('.tag-row').remove();

				// show "no tags" row in table
				$('#no-tags-message').removeClass('hidden');
				
			}
			else{
				console.log(res.message);
			}
		});
	});

});