$(document).ready(function(){



	$('.deletetag').on('click', function(){


		var confirmDelete = confirm("If you delete a tag you will remove it form any comments it is currently attached to. \n Are you sure you want to delete it?");
		if(!confirmDelete)
			return;

		var tagRow = $(this).parent().closest('tr');
		var tagID = tagRow.attr('tagid');

		// run ajax to remore tag
		$.ajax({
			method: "GET",
			url: "ajaxdeletetag",
			data: { tagid: tagID },
		}).done(function (res) {
			if(res.success){

				// remove row
				tagRow.remove();

				// show no rows message if no more tags left in table
				if($('.tagrow').length <= 0) {
					$('#norowsmessage').removeClass('hidden');
				}
			}
			else{
				console.log(res.message);
			}
		});
	});


});