<?php
/**
 * Routes - all standard routes are defined here.
 *
 * @author David Carr - dave@daveismyname.com
 * @version 2.2
 * @date updated Sept 19, 2015
 */

/** Create alias for Router. */
use Core\Router;
use Helpers\Hooks;

/** Define routes. */
/** home */
Router::any('', 'Controllers\Home@Home');
Router::any('home', 'Controllers\Home@loggedIn');
Router::any('logout', 'Controllers\Home@logout');

/** comments */
Router::any('allcomments', 'Controllers\Comments@allComments');
Router::any('untaggedcomments', 'Controllers\Comments@untaggedComments');
Router::any('tagcomment', 'Controllers\Comments@tagComment');

/** admin */
Router::any('tagmanagement', 'Controllers\Admin@tagManagement');
Router::any('locationmanagement', 'Controllers\Admin@locationManagement');
Router::any('export', 'Controllers\Admin@export');
Router::any('exportexcel', 'Controllers\Export@exportExcel');

/** User */
Router::any('createuser', 'Controllers\User@createUser');
Router::any('manageuser', 'Controllers\User@manageUser');

/* Analysis */
Router::any('tagoverview', 'Controllers\Analysis@tagOverview');
Router::any('taganalysis', 'Controllers\Analysis@tagAnalysis');



/** AJAX routes. */
Router::any('ajaxaddtag', 'Models\Tags@ajaxAddTag');
Router::any('ajaxremovetag', 'Models\Tags@ajaxRemoveTag');
Router::any('ajaxremovealltags', 'Models\Tags@ajaxRemoveAllTags');
Router::any('ajaxdeletetag', 'Models\Tags@ajaxDeleteTag');
Router::any('ajaxdeletelocation', 'Models\Comments@ajaxDeleteLocation');



/** Module routes. */
$hooks = Hooks::get();
$hooks->run('routes');

/** If no route found. */
Router::error('Core\Error@index');

/** Turn on old style routing. */
Router::$fallback = false;

/** Execute matched routes. */
Router::dispatch();
