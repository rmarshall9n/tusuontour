<?php

namespace Models;
	
use Core\Model;

/**
 * Class login
 * handles the user's login and logout process
 */
class User extends Model
{

	/**
	 * @var array Collection of error messages
	 */
	public $errors = array();
	/**
	 * @var array Collection of success / neutral messages
	 */
	public $messages = array();

	/**
	 * the function "__construct()" automatically starts whenever an object of this class is created,
	 * you know, when you do "$login = new Login();"
	 */
	public function __construct()
	{

		parent::__construct();
	}


	public function createUser($username, $password, $passwordRepeat, $firstname, $surname, $email)
	{

		// data for validation and insert
		$data = array(
			'username' => $username,
			'password' => $password,
			'passwordrepeat' => $passwordRepeat,
			'email' => $email,
			'firstname' => $firstname,
			'surname' => $surname
		);

		// validate input
		$isValid = \Helpers\Gump::is_valid($data, array(
			'username' => 'required|alpha_numeric|min_len, 2|max_len, 64',
			'password' => 'required|min_len, 6',
			'passwordrepeat' => 'required|equalsfield, password',
			'email' => 'required|valid_email|max_len, 64',
			'firstname' => 'required|min_len, 2',
			'surname' => 'required|min_len, 2'
		));
		
		// If invalid, return error message
		if (!$isValid) {
			return $isValid;
		}

		// sanitise input
		$data = \Helpers\Gump::sanitize($data);

		// create a secure password hash
		$passwordHash = \Helpers\Password::make($data['password']);

		$sqlData = array('username'=>$data['username'], 'email' => $data['email']);
		// check if user or email address already exists
		$sql = "SELECT * FROM users WHERE user_name = :username OR user_email = :email";
		$existingUsers = $this->db->select($sql, $sqlData);

		if (count($existingUsers) <= 0) {
			// Change this when you have updated the database
			$insertData = array('user_name'=> $data['username'], 'user_password_hash'=>$passwordHash, 'user_email'=>$data['email']);
			$insert = $this->db->insert('users', $insertData);

			// if user has been added successfully
			if ($insert) {
				//"Your account has been created successfully. You can now log in.";
			} else {
				// "Sorry, your registration failed. Please go back and try again.";
			}
		} else {
			//user name or email already exists
		}
	}

	public function updatePassword($userID, $oldPassword, $newPassword, $passwordRepeat)
	{

		// data for validation and insert
		$data = array(
			'userID' => $userID,
			'oldPassword' => $oldPassword,
			'newPassword' => $newPassword,
			'passwordRepeat' => $passwordRepeat,
		);

		// validate input
		$isValid = \Helpers\Gump::is_valid($data, array(
			'userID' => 'required|integer',
			'oldPassword' => 'required',
			'newPassword' => 'required|min_len, 6',
			'passwordRepeat' => 'required|equalsfield, newPassword',
		));

		// If invalid, return error message
		if (!$isValid) {
			return $isValid;
		}


		// select the current user
		$sql = "SELECT user_password_hash FROM users WHERE user_id = :userid";
		$selectData = array('userid' => $userID);
		$currentUser = $this->db->select($sql, $selectData);

		// check if the current users password matchs their input
		$verified = \Helpers\Password::verify($oldPassword, $currentUser[0]['user_password_hash']);

		// if not verified, return error
		if (!$verified) {
			return false;// "Incorrect password.";
		}
		// create a secure password hash
		$passwordHash = \Helpers\Password::make($data['newPassword']);
		$updateData = array('user_password_hash' => $passwordHash);
		$whereData = array('user_id' => $userID);
		
		// Change this when you have updated the database
		$update = $this->db->update('users', $updateData, $whereData);
	}

	/**
	 * log in with post data
	 */
	public function attemptLogin($username, $password)
	{

		// check login form contents
		if (empty($username)) {
			$this->errors[] = "Username field was empty.";
		} elseif (empty($password)) {
			$this->errors[] = "Password field was empty.";
		} else {
			// database query, getting all the info of the selected user (allows login via email address in the
			// username field)
			$sql = "SELECT user_id, user_name, user_email, user_password_hash
					FROM users
					WHERE user_name = :username OR user_email = :useremail";

			$data = array('username' => $username, 'useremail' => $userEmail);


			$result = $this->db->select($sql, $data);

			// if this user exists
			if (count($result) == 1) {
				// get result row (as an object)
				$result = $result[0];

				// using PHP 5.5's password_verify() function to check if the provided password fits
				// the hash of that user's password
				if (\Helpers\Password::verify($password, $result['user_password_hash'])) {
					session_start();
					// write user data into PHP SESSION (a file on your server)
					$_SESSION['username'] = $result['user_name'];
					$_SESSION['userID'] = $result['user_id'];
					$_SESSION['userEmail'] = $result['user_email'];
					$_SESSION['userLoginStatus'] = 1;
				} else {
					$this->errors[] = "Wrong password. Try again.";
				}
			} else {
				$this->errors[] = "This user does not exist.";
			}
		   
		}
	}

	/**
	 * perform the logout
	 */
	public function logout()
	{
		// delete the session of the user
		$_SESSION = array();
		session_destroy();
		// return a little feeedback message
		$this->messages[] = "You have been logged out.";

	}

	/**
	 * simply return the current state of the user's login
	 * @return boolean user's login status
	 */
	public function isUserLoggedIn()
	{
		if (isset($_SESSION['userLoginStatus']) and $_SESSION['userLoginStatus'] == 1) {
			return true;
		}
		// default return
		return false;
	}
}
