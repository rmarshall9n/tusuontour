<?php
namespace Models;

use Core\Model;

use Helpers\AjaxHandler as Ajax;

class Analysis extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getTopTags($num, $characteristic = -1)
	{

		$countSQL = "";
		$data[':num'] = $num;

		// if characteristic = -1 get all
		if ($characteristic > -1) {
			$data[':characteristicid'] = $characteristic;
			$countSQL = ", SUM(CASE WHEN taggedcomments.characteristicID = :characteristicid THEN 1 ELSE 0 END) total";
		} else {
			$countSQL = ", COUNT(*) AS total";
		}

		$sql = "SELECT taggedcomments.tagID, tags.tagName
                $countSQL
                FROM taggedcomments
                LEFT JOIN tags ON tags.tagID = taggedcomments.tagID
                WHERE tags.hidden = 0
				GROUP BY taggedcomments.tagID ORDER BY total DESC LIMIT :num";

		$results = $this->db->select($sql, $data);

		return $results;
	}


	public function getTagInfo($tagID)
	{

		$data[':tagid'] = $tagID;

		$sql = "SELECT taggedcomments.tagID, tags.tagName, tags.parentID, COUNT(*) AS totalCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 2 THEN 1 ELSE 0 END) totalNegativeCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 1 THEN 1 ELSE 0 END) totalPositiveCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 0 THEN 1 ELSE 0 END) totalNeutralCount
                FROM taggedcomments
                LEFT JOIN tags ON tags.tagID = taggedcomments.tagID
                WHERE tags.hidden = 0 AND tags.tagID = :tagid
				GROUP BY taggedcomments.tagID ORDER BY tags.tagName";

		$results = $this->db->select($sql, $data);

		if (!empty($results)) {
			return $results[0];
		} else {
			return array();
		}
	}


	public function getTagsPerMonth($numMonths, $tagID)
	{

		$monthsAgo = $numMonths - 1;
		$startDate = strtotime("first day of -".$monthsAgo." months");

		$data = array('startdate' => $startDate, 'tagid' => $tagID);

		$sql = "SELECT taggedcomments.tagID, COUNT(*) AS total, FROM_UNIXTIME(comments.timestamp, '%b') as month,
                    SUM(CASE WHEN taggedcomments.characteristicID = 2 THEN 1 ELSE 0 END) negativeCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 1 THEN 1 ELSE 0 END) positiveCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 0 THEN 1 ELSE 0 END) neutralCount
                FROM taggedcomments
                LEFT JOIN tags ON tags.tagID = taggedcomments.tagID
                LEFT JOIN comments ON comments.commentID = taggedcomments.commentID
                WHERE tags.hidden = 0 AND comments.timestamp > :startdate AND taggedcomments.tagID = :tagid
				GROUP BY month ORDER BY comments.timestamp ASC";

		$results = $this->db->select($sql, $data);

		$currentMonth = 0;
		$return = array();
		for ($i = $monthsAgo; $i >= 0; $i--) {
			$month = date('M', strtotime("first day of -".$i." months"));
			
			if ($month == $results[$currentMonth]['month']) {
				$return[$i] = $results[$currentMonth];
				$currentMonth++;
			} else {
				$return[$i] = array(
					'tagID' => $tagID,
				    'total' => 0,
				    'month' => $month,
				    'negativeCount' => 0,
				    'positiveCount' => 0,
				    'neutralCount' => 0
				    );
			}
		}
		return $return;
	}
}
