<?php
namespace Models;

use Core\Model;

use Helpers\AjaxHandler as Ajax;

class Tags extends Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function getTagsByCommentID($commentID, $includeHidden = 0)
	{

		// select all tags related to comment, depending on hidden
		$sql = "SELECT * FROM taggedcomments 
                LEFT JOIN tags ON tags.tagID = taggedcomments.tagID
                LEFT JOIN characteristics ON characteristics.characteristicID = taggedcomments.characteristicID
				WHERE commentID = :commentID AND tags.hidden = :hidden";
		
		$data = array(
			'commentID' => $commentID,
			'hidden' => $includeHidden);

		$results = $this->db->select($sql, $data);
		
		if (!empty($results)) {
			return $results;
		} else {
			return array();
		}
	}

	public function getTagByID($tagID, $includeHidden = 0)
	{

		// Select tag by its id
		$sql = "SELECT * FROM tags 
				WHERE tagID = :tagid AND tags.hidden = :hidden";
		
		$data = array(
			'tagid' => $tagID,
			'hidden' => $includeHidden);

		$results = $this->db->select($sql, $data);
		
		if (!empty($results)) {
			return $results[0];
		} else {
			return array();
		}
	}

	public function getTags()
	{

		$sql = "SELECT * FROM tags WHERE hidden = 0 AND parentID = -1 ORDER BY tagName";
		$headingResult = $this->db->select($sql);

		foreach ($headingResult as $key => $row) {
			$sql = "SELECT * FROM tags WHERE hidden = 0 AND parentID = ".$row['tagID']." ORDER BY tagName";
			$headingResult[$key]['subTags'] = $this->db->select($sql);
		}

		return $headingResult;
	}

	public function getTagCharacteristicCounts()
	{

		$sql = "SELECT taggedcomments.tagID, tags.tagName, tags.parentID, COUNT(*) AS total,
                    SUM(CASE WHEN taggedcomments.characteristicID = 2 THEN 1 ELSE 0 END) negativeCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 1 THEN 1 ELSE 0 END) positiveCount,
                    SUM(CASE WHEN taggedcomments.characteristicID = 0 THEN 1 ELSE 0 END) neutralCount
                FROM taggedcomments
                LEFT JOIN tags ON tags.tagID = taggedcomments.tagID
                WHERE tags.hidden = 0
				GROUP BY taggedcomments.tagID ORDER BY tags.tagName";

		$results = $this->db->select($sql);

		if (!empty($results)) {
			return $results;
		} else {
			return array();
		}
	}

	public function getAllTagsForComments()
	{

		$sql = "SELECT taggedcomments.*, comments.timestamp, tags.tagName, locations.locationName, characteristics.characteristic 
                FROM taggedcomments 
                    LEFT JOIN comments ON comments.commentID = taggedcomments.commentID
                    LEFT JOIN tags ON tags.tagID = taggedcomments.tagID
                    LEFT JOIN locations ON locations.locationID = comments.locationID
                    LEFT JOIN characteristics ON characteristics.characteristicID = taggedcomments.characteristicID
                WHERE tags.hidden = 0
				ORDER BY comments.timestamp";

		$results = $this->db->select($sql);

		return $results;
	}

	public function createTag($tagName, $parentID)
	{

		if (empty($tagName)) {
			return -1;
		}

		// attempt to insert tag
		$data = array("tagName" => $tagName, "parentID" => $parentID);
		$tagID = $this->db->insert("tags", $data);

		return $tagID;
	}

	public function deleteTag($tagID)
	{

		// create data array
		$data = array("tagID" => $tagID);
		
		// delete the entry
		$rowCount = $this->db->delete("tags", $data);

		return $rowCount;
	}

	public function getCharacteristics()
	{
		
		return $this->db->select('SELECT * FROM characteristics');
	}








	public function ajaxAddTag()
	{

		// get data passed through ajax
		$commentID = Ajax::get("commentid");
		$tagID = Ajax::get("tagid");
		$characteristicID = Ajax::get("characteristicid");

		// create data array
		$data = array("commentID" => $commentID, "tagID" => $tagID, "characteristicID" => $characteristicID);
		
		// run select statement to see if tag already exists for comment
		$sql = "SELECT * FROM taggedcomments WHERE commentID = :commentID AND tagID = :tagID AND characteristicID = :characteristicID";
		$result = $this->db->select($sql, $data);
		
		if (count($result) <= 0) {
			// attempt to insert tag
			$this->db->insert("taggedcomments", $data);

			// if a valid id was returned, success
			if ($taggedCommentID >= 0) {
				Ajax::success($return);
			} // else error
			else {
				Ajax::error('Database error inserting tag.');
			}
		}

		Ajax::error('Tag already exists.', array(), 200);
	}

	public function ajaxRemoveTag()
	{

		// get data passed through ajax
		$commentID = Ajax::get("commentid");
		$tagID = Ajax::get("tagid");
		$characteristicID = Ajax::get("characteristicid");
		
		// create data array
		$data = array("commentID" => $commentID, "tagID" => $tagID, "characteristicID" => $characteristicID);
		
		// delete the entry
		$rowCount = $this->db->delete("taggedcomments", $data);

		// if a row count of , success
		if ($rowCount > 0) {
			Ajax::success($rowCount);
		} else {
			Ajax::error('Database error removing tag.', array(), 200);
		}
	}

	public function ajaxRemoveAllTags()
	{

		// get data passed through ajax
		$commentID = Ajax::get("commentid");

		// create data array
		$data = array("commentID" => $commentID);
		
		// delete the entry
		$rowCount = $this->db->delete("taggedcomments", $data);
		
		// if a row count of , success
		Ajax::success($rowCount);
	}



	public function ajaxCreateTag()
	{

		$tagName = Ajax::get("tagname");
		$parentID = Ajax::get("parentid");

		$newID = self::CreateTag($tagName, $parentID);

		// if a valid id was returned, success
		if ($newID >= 0) {
			Ajax::success("Success.");
		} else {
			Ajax::error('Database error creating tag.');
		}
	}

	public function ajaxDeleteTag()
	{

		$tagID = Ajax::get("tagid");

		$numRows = self::DeleteTag($tagID);

		Ajax::success("Success.");
		
	}
}
