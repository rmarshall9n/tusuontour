<?php
namespace Models;
	
use Core\Model;
use Helpers\AjaxHandler as Ajax;

class Comments extends Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getComment($commentID)
	{

		$sql =
			"SELECT * FROM comments 
             LEFT JOIN locations ON comments.locationID = locations.locationID
			 WHERE commentID = :commentid AND softDeleted=0";
		
		$data = array('commentid' => $commentID);
		
		$results = $this->db->select($sql, $data);

		if (!empty($results)) {
			$results[0]['date'] = date('d/m/Y', $results[0]['timestamp']);
			return $results[0];
		} else {
			return -1;
		}
	}

	public function getCommentsWithTagID($tagID)
	{

		$sql = "SELECT comments.*, locations.locationName, characteristics.characteristic,
                FROM_UNIXTIME(comments.timestamp, '%D %M %Y') as month
                FROM comments
                LEFT JOIN taggedcomments ON taggedcomments.commentID = comments.commentID
                LEFT JOIN locations ON comments.locationID = locations.locationID
                LEFT JOIN characteristics ON characteristics.characteristicID = taggedcomments.characteristicID
			 	WHERE taggedcomments.tagID = :tagid AND softDeleted=0";

		$data = array('tagid' => $tagID);
		
		$results = $this->db->select($sql, $data);

		return $results;
	}

	public function getComments($startTimestamp = 0, $endTimestamp = 0)
	{

		$data = array(
					'startimestamp' => $startTimestamp,
					'endTimestamp' => $endTimestamp);

		$dateSQL = "";
		if ($startTimestamp > 0 && $endTimestamp > 0 && $startTimestamp <= $endTimestamp) {
			$dateSQL = "timestamp >= :startimestamp AND timestamp < :endTimestamp AND ";
		} elseif ($startTimestamp == 0 && $endTimestamp < 0)
			$dateSQL = "timestamp < :endTimestamp AND ";
		elseif ($endTimestamp == 0 && $startTimestamp < 0)
			$dateSQL = "timestamp >= :startimestamp AND ";

		$sql = "SELECT * FROM comments 
                LEFT JOIN locations ON comments.locationID = locations.locationID
				WHERE ".$dateSQL."softDeleted = 0";
				
		return $this->db->select($sql, $data);
	}

	public function getNextUntaggedComment($commentID)
	{

		$sql = "SELECT comments.commentID FROM comments
                LEFT JOIN taggedcomments ON taggedcomments.commentID = comments.commentID
                WHERE taggedcomments.commentID IS NULL AND comments.softDeleted = 0 AND comments.commentID > :commentID
				ORDER BY comments.commentID LIMIT 1";

		$data = array('commentID' => $commentID);
		$result = $this->db->select($sql, $data);

		if (count($result) <= 0) {
			$sql = "SELECT comments.commentID FROM comments
                LEFT JOIN taggedcomments ON taggedcomments.commentID = comments.commentID
                WHERE taggedcomments.commentID IS NULL AND comments.softDeleted = 0 AND comments.commentID > 0
				ORDER BY comments.commentID LIMIT 1";

			$result = $this->db->select($sql);
		}


		if (count($result)) {
			$result = $result[0]['commentID'];
		} else {
			$result = -1;
		}

		return $result;
	}

	public function addComment($studentNumber, $comment, $locationID)
	{
		
		// validate input
		$is_valid = \Helpers\Gump::is_valid($_REQUEST, array(
			'studentNumber' => '',
			'comment' => 'required',
			'locationID' => 'required'
		));
			
		$timestamp = time();
		
		$data = array(
			'studentNumber' => $studentNumber,
			'comment' => $comment,
			'locationID' => $locationID,
			'timestamp' => $timestamp);

		$result = $this->db->insert('comments', $data);

		return $result;
	}

	public function getUntaggedComments()
	{

		$sql = "SELECT * FROM comments 
                WHERE comments.commentID NOT IN (SELECT taggedcomments.commentID FROM taggedcomments WHERE taggedcomments.commentID IS NOT NULL)
				AND softDeleted=0";

		return $this->db->select($sql);
	}
	
	public function getCommentsWithTag($tagID, $characteristicID)
	{

		$data = array(
			'tagID' => $tagID,
			'characteristicID' => $characteristicID);

		$sql =
			"SELECT comments.*, locations.* FROM taggedcomments
            LEFT JOIN comments ON comments.commentID = taggedcomments.commentID 
            LEFT JOIN locations ON comments.locationID = locations.locationID
			WHERE tagID = :tagID AND characteristicID = :characteristicID AND comments.softDeleted = 0";

		return $this->db->select($sql, $data);
	}

	public function deleteComment($commentID)
	{

		$data = array('softDeleted' => 1);
		$where = array('commentID' => $commentID);

		$this->db->update('comments', $data, $where);
		$this->db->delete('taggedcomments', $where);
		return;
	}

	public function getLocations()
	{

		return $this->db->select('SELECT * FROM locations ORDER BY locationName');
	}

	public function createLocation($locationName)
	{

		$data = array('locationName' => $locationName);

		// validate input
		$isValid = \Helpers\Gump::is_valid($data, array(
			'locationName' => 'required|alpha_numeric'
		));
		
		if (!$isValid) {
			return -1;
		}

		$result = $this->db->insert('locations', $data);

		return $result;
	}

	public function deleteLocation($locationID)
	{

		$where = array('locationID' => $locationID);

		// validate input
		$isValid = \Helpers\Gump::is_valid($data, array(
			'locationID' => 'required|numeric'
		));
		
		if (!$isValid) {
			return -1;
		}
		$result = $this->db->delete('locations', $where);
		
		return $result;
	}

	public function ajaxDeleteLocation()
	{

		$locationID = Ajax::Get("locationid");

		$numRows = self::DeleteLocation($locationID);

		Ajax::success("Success.");
	}
}
